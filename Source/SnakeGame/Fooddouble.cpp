// Fill out your copyright notice in the Description page of Project Settings.


#include "Fooddouble.h"
#include "SnakeBase.h"

// Sets default values
AFooddouble::AFooddouble()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFooddouble::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFooddouble::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFooddouble::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(2);
			this->Destroy();
		}
	}
}

